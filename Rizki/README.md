# Hello my name's Rizki👋
Hello, I'm Rizki. First of all, thank you to my colleagues at MIKTI, especially Coach Helmi, and nice to meet everyone. I'm very enthusiastic about learning Golang, and hopefully, everyone will achieve what they desire in this program.

## About Me
- 👋 Hi, I’m Rizki, a collage student from Suryakancana University.
- 👀 I’m interested in Backend, AI, Web Development, Cloud Computing and etc ..
- 🌱 I’m currently learning Backend
- 😅 Say hello

## Let's Connect
- github : https://github.com/shiirookami